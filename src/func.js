const getSum = (str1, str2) => {
  if(typeof str1 != 'string') return false;   
  if(isNaN(str1) || isNaN(str2)) return false;   
  let sum = 0;
  sum = +str1 + +str2;
  return String(sum);                
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let arr = listOfPosts.filter(function(item) {
    return item.author === authorName;
  })
  let arrCom = listOfPosts.map(function(elem) {
    return elem.comments;
  })  
  let newArr = [].concat(arrCom[0], arrCom[1]);
  let arrNew = newArr.filter(function(comm) {
    return comm.author === authorName;
  })  
  let res = `Post:`+`${arr.length}`+`,comments:`+`${arrNew.length}`;
  return res;
}

const tickets=(people)=> {
  let arr = people;
  let balance = 0;
  let price = 25;
  for(let ar of arr) {
    if(ar === price) {
      balance +=ar;
      continue;
    }
    balance += balance - (ar - price);
  }
  if(balance >= 0) return `YES`; else return `NO`;
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
